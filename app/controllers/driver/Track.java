package controllers.driver;

import models.Driver;
import models.PackageStatus;
import models.PackageStatusName;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import tyrex.util.ArraySet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Security.Authenticated(Secured.class)
public class Track extends Controller {
    public static Result viewAll() {
        models.Driver driver = models.Driver.findByLogin(session("login"));
        List<models.Track> tracks = models.Track.find.where().eq("driver_id", driver.id).findList();
        List<models.Track> newTracks = new ArrayList<>();

        for (models.Track track : tracks) {
            boolean done = true;
            for (models.Package pack : track.packages) {
                if (pack.statuses.size() < 6) done = false;
            }
            if (!done) {
                newTracks.add(track);
            }
        }


        return ok(views.html.driver.tracks.render(newTracks));
    }

    public static Result view( int id ){
        models.Track track = models.Track.findById(id);

        return ok(views.html.driver.track.render(track));
    }

    public static Result changeStatus( int id, int packageId ) {
        models.Package pack = models.Package.findById(packageId);
        if (pack != null) {
            if (pack.statuses.size() < 6) {
                PackageStatus status = new PackageStatus();
                status.date = new Date();
                status.name = PackageStatusName.values()[pack.statuses.size()];
                status.package_ = pack;
                status.save();
            }
            // TODO
            //pack.order.orderStatus.
        }
        return redirect(request().getHeader("referer"));
    }
}
