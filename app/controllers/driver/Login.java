package controllers.driver;

import controllers.routes;
import play.data.validation.ValidationError;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;

public class Login extends Controller {

    public static Result login() {
        return ok(views.html.driver.login.render(Form.form(LoginValidator.class)));
    }

    public static Result logout() {
        session().clear();
        return redirect(routes.Application.index());
    }

    public static Result authenticate() {
        Form<LoginValidator> loginForm = Form.form(LoginValidator.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(views.html.driver.login.render(loginForm));
        } else {
            setLoggedDriver(loginForm.get().login);
            return redirect(controllers.driver.routes.Dashboard.index());
        }
    }

    public static void setLoggedDriver( String login ) {
        session().clear();
        session("login", login);
        session("who", "driver");
    }

    public static class LoginValidator {
        public String login;
        public String password;

        public List<ValidationError> validate() {
            List<ValidationError> errors = new ArrayList<>();

            models.Driver driver = models.Driver.findByLogin(login);
            if (driver == null) errors.add(new ValidationError("login", "Nieprawidłowy login"));
            else {
                if (!driver.password.equals(password)) errors.add(new ValidationError("password", "Nieprawidłowe hasło"));
            }
            if (!errors.isEmpty()) return errors;

            return null;
        }
}}
