package controllers.driver;

import controllers.routes;
//import controllers.admin.routes;
import models.*;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.List;

@Security.Authenticated(Secured.class)
public class Dashboard extends Controller {
    public static Result index() {
        Driver driver = Driver.findByLogin(session("login"));
        List<models.Track> tracks = models.Track.find.where().eq("driver_id", driver.id).orderBy("id desc").findList();

        for (models.Track track : tracks) {
            boolean done = true;
            for (models.Package pack : track.packages) {
                if (pack.statuses.size() < 6) done = false;
            }
            if (!done) return redirect(controllers.driver.routes.Track.view(track.id));
        }

        return ok(views.html.driver.index.render(driver));
    }
}
