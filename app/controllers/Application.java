package controllers;

import controllers.client.*;
import models.City;
import models.Client;
import models.Street;
import play.*;
import play.data.Form;
import play.db.ebean.Model;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.*;

import views.html.*;

import javax.persistence.Query;
import java.util.List;

public class Application extends Controller {
    public static Result index() {
        if (session("who") != null) {
            String who = session("who");
            if (who.equals("client")) return redirect(controllers.client.routes.Dashboard.index());
            if (who.equals("admin")) return redirect(controllers.admin.routes.Dashboard.index());
            if (who.equals("driver")) return redirect(controllers.driver.routes.Dashboard.index());
        }
        return ok(index.render());
    }
}
