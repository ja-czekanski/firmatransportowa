package controllers.admin;

import controllers.client.*;
import models.*;
import models.Package;
import models.Track;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import tyrex.util.ArraySet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Security.Authenticated(Secured.class)
public class Driver extends Controller {
    public static Result viewAll() {
        List<models.Driver> drivers = models.Driver.find.all();
        Set<Integer> used = new ArraySet();

        List<models.Track> tracks = models.Track.find.all();
        for (models.Driver driver : drivers) {
            for (models.Track track : tracks) {
                if (track.driver.id == driver.id) {
                    used.add(driver.id);
                    break;
                }
            }
        }
        return ok(views.html.admin.drivers.render(drivers, used));
    }

    public static Result view( int id ){
        models.Driver driver = models.Driver.findById(id);
        List<models.Track> tracks = Track.find.where().eq("driver_id", driver.id).findList();
        return ok(views.html.admin.driverInfo.render(driver, tracks));
    }

    public static Result addForm() {
        Form<models.Driver> form = Form.form(models.Driver.class);
        return ok(views.html.admin.newDriverForm.render(form));
    }

    public static Result add() {
        Form<models.Driver> form = Form.form(models.Driver.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(views.html.admin.newDriverForm.render(form));
        } else {
            models.Driver driver = form.get();
            driver.save();

            flash("message", "Kierowca stworzony");
            return redirect(controllers.admin.routes.Driver.viewAll());
        }
    }

    public static Result delete(int id) {
        models.Driver.findById(id).delete();
        return redirect(request().getHeader("referer"));
    }
}
