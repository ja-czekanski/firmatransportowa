package controllers.admin;

import controllers.routes;
import play.data.Form;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;

public class Login extends Controller {

    public static Result login() {
        return ok(views.html.admin.login.render(Form.form(LoginValidator.class)));
    }

    public static Result logout() {
        session().clear();
        return redirect(routes.Application.index());
    }

    public static Result authenticate() {
        Form<LoginValidator> loginForm = Form.form(LoginValidator.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(views.html.admin.login.render(loginForm));
        } else {
            setLoggedAdmin(loginForm.get().login);
            return redirect(controllers.admin.routes.Dashboard.index());
        }
    }

    public static void setLoggedAdmin( String login ) {
        session().clear();
        session("login", login);
        session("who", "admin");
    }

    public static class LoginValidator {
        public String login;
        public String password;

        public List<ValidationError>  validate() {
            List<ValidationError> errors = new ArrayList<>();

            models.Administrator admin = models.Administrator.findByLogin(login);
            if (admin == null) errors.add(new ValidationError("login", "Nieprawidłowy login"));
            else {
                if (!admin.password.equals(password)) errors.add(new ValidationError("password", "Nieprawidłowe hasło"));
            }
            if (!errors.isEmpty()) return errors;

            return null;
        }
    }
}
