package controllers.admin;

import controllers.client.*;
import models.*;
import models.Package;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import tyrex.util.ArraySet;

import java.util.*;

@Security.Authenticated(Secured.class)
public class PackageType extends Controller {
    public static Result viewAll() {
        List<models.PackageType> packageTypes = models.PackageType.find.all();
        Set<Integer> used = new ArraySet();

        List<Package> packages = Package.find.all();
        for (models.PackageType type : packageTypes) {
            for (Package pack : packages) {
                if (pack.packageType.id == type.id) {
                    used.add(type.id);
                    break;
                }
            }
        }
        return ok(views.html.admin.packageTypes.render(packageTypes, used));
    }

    public static Result view(int id) {
        Form<models.PackageType> form = Form.form(models.PackageType.class);
        models.PackageType packageType = models.PackageType.findById(id);
        form = form.fill(packageType);
        return ok(views.html.admin.packageTypeForm.render(form));
    }

    public static Result add() {
        Form<models.PackageType> form = Form.form(models.PackageType.class);
        models.PackageType packageType = new models.PackageType();
        packageType.save();
        form = form.fill(packageType);
        return ok(views.html.admin.packageTypeForm.render(form));
    }

    public static Result edit() {
        Form<models.PackageType> form = Form.form(models.PackageType.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(views.html.admin.packageTypeForm.render(form));
        } else {
            models.PackageType packageType = form.get();
            packageType.update();

            return redirect(controllers.admin.routes.PackageType.viewAll());
        }
    }

    public static Result delete(int id) {
        models.PackageType.findById(id).delete();
        return redirect(request().getHeader("referer"));
    }
}