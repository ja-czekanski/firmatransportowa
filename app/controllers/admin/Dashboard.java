package controllers.admin;

import controllers.client.Secured;
import models.Administrator;
import models.Client;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

@Security.Authenticated(controllers.admin.Secured.class)
public class Dashboard extends Controller {
    public static Result index() {
        Administrator admin = Administrator.findByLogin(session("login"));
        return ok(views.html.admin.index.render(admin));
    }
}
