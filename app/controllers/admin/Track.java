package controllers.admin;

import models.*;

import models.Order;
import models.Package;
import models.PackageType;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Security.Authenticated(Secured.class)
public class Track extends Controller {
    public static Result viewAll() {
        List<models.Track> tracks = models.Track.find.all();
        return ok(views.html.admin.tracks.render(tracks));
    }

    public static Result view(int id) {
        models.Track track = models.Track.find.where().eq("id", id).findUnique();
        if (track == null) return redirect(routes.Track.viewAll());

        return ok(views.html.admin.track.render(track));
    }

    public static Result addForm() {
        DynamicForm form = Form.form();
        return ok(views.html.admin.newTrackForm.render(form));
    }

    public static Result add() {
        DynamicForm form = Form.form().bindFromRequest();
        models.Track track = new models.Track();

        String driver_id = form.get("driver_id");
        if (driver_id != null) {
            int driverId = 0;
            try {
                driverId = Integer.parseInt(driver_id);

                track.driver = models.Driver.findById(driverId);
                if (track.driver == null) form.reject("driver_id", "Nieprawidłowy kierowca");
            } catch (NumberFormatException e) {
                form.reject("driver_id", "Wybierz kierowcę");
            }
        }
        else form.reject("driver_id", "Wybierz kierowcę");

        if (form.hasErrors()) {
            return badRequest(views.html.admin.newTrackForm.render(form));
        } else {
            track.trackStatus = TrackStatus.NEW;
            track.save();

            flash("message", "Trasa utworzona");
            return redirect(controllers.admin.routes.Track.view(track.id));
        }
    }

    public static Result delete(int id) {
        models.Track track = models.Track.findById(id);

        if (track != null) {
            if (track.packages != null) {
                for (models.Package pack : track.packages) {
                    pack.track = null;
                    pack.save();
                }
            }

            track.delete();
        }
        return redirect(request().getHeader("referer"));
    }

    public static Result accept(int id) {
        models.Track track = models.Track.findById(id);

        if (track != null) {
            if (track.packages != null) {
                for ( models.Package pack : track.packages) {
                    PackageStatus status = new PackageStatus();
                    status.date = new Date();
                    status.name = PackageStatusName.WAITINGFORCLIENT;
                    status.package_ = pack;
                    status.save();
                }
                track.trackStatus = TrackStatus.READY;
                track.save();
            }
        }
        return redirect(request().getHeader("referer"));
    }

    public static Result addPackageForm( int id ) {
        DynamicForm form = Form.form();
        return ok(views.html.admin.addPackageToTrack.render(form, id));
    }

    public static Result addPackage( int id ) {
        DynamicForm form = Form.form().bindFromRequest();

        String package_id = form.get("package_id");
        if (package_id != null) {
            int packageId = 0;
            try {
                packageId = Integer.parseInt(package_id);

                Package pack = Package.findById(packageId);
                if (pack != null) {
                    pack.track = models.Track.findById(id);
                    pack.update();
                }
                else form.reject("package_id", "Nieprawidłowa przesyłka");
            } catch (NumberFormatException e) {
                form.reject("package_id", "Wybierz przesyłkę");
            }
        }
        else form.reject("package_id", "Wybierz przesyłkę");

        if (form.hasErrors()) {
            return badRequest(views.html.admin.addPackageToTrack.render(form, id));
        } else {
            flash("message", "Przesyłka dodana");
            return redirect(controllers.admin.routes.Track.view(id));
        }
    }

    public static Result removePackage( int packageId ) {
        Package pack = Package.findById(packageId);
        if (pack != null) {
            pack.track = null;
            pack.update();
        }
        return redirect(request().getHeader("referer"));
    }
}