package controllers.admin;

import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.List;

@Security.Authenticated(controllers.admin.Secured.class)
public class Client extends Controller {
    public static Result viewAll() {
        List<models.Client> clientList = models.Client.find.all();
        return ok(views.html.admin.clients.render(clientList));
    }

    public static Result view( int id ) {
        models.Client client = models.Client.findById(id);
        return ok(views.html.admin.clientInfo.render(client));
    }
}
