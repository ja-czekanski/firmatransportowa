package controllers.admin;

import models.Order;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.List;

@Security.Authenticated(Secured.class)
public class Invoice extends Controller {
    public static Result viewAll() {
        List<models.Order> invoices = Order.find.where().isNotNull("invoice_number").findList();
        return ok(views.html.admin.invoices.render(invoices));
    }

    public static Result view( int id ) {
        Order order = Order.findById(id);
        return ok(views.html.shared.invoice.render(order));
    }
}
