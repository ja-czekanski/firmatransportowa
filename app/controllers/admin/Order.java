package controllers.admin;

import models.OrderStatus;
import models.PackageStatus;
import models.PackageStatusName;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.Date;
import java.util.List;

@Security.Authenticated(Secured.class)
public class Order extends Controller {
    public static Result viewAll() {
        List<models.Order> orders = models.Order.find.all();
        return ok(views.html.admin.orders.render(orders));
    }

    public static Result view( int id ) {
        models.Order order = models.Order.findById(id);
        return ok(views.html.admin.orderInfo.render(order));
    }

    public static Result accept(int id) {
        models.Order order = models.Order.findById(id);
        if (order.orderStatus == OrderStatus.READY) {
            order.orderStatus = OrderStatus.DONE;
            order.save();
        }
        return redirect(request().getHeader("referer"));
    }
}
