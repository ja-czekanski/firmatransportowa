package controllers.utils;

import controllers.client.Secured;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.List;

@Security.Authenticated(controllers.admin.Secured.class)
public class Driver extends Controller {
    public static Result getDrivers() {
        List<models.Driver> drivers = models.Driver.find.all();
        return ok(Json.toJson(drivers));
    }
}
