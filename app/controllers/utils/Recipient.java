package controllers.utils;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.List;

@Security.Authenticated(controllers.client.Secured.class)
public class Recipient extends Controller {
    public static Result getRecipiens() {
        List<models.Recipient> recipients = models.Recipient.find.all();
        return ok(Json.toJson(recipients));
    }
}
