package controllers.utils;

import models.OrderStatus;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.Iterator;
import java.util.List;

@Security.Authenticated(controllers.admin.Secured.class)
public class Package extends Controller {
    public static Result getPackages() {
        List<models.Package> packages = models.Package.find.where().isNull("track_id").findList();

        for (Iterator<models.Package> it = packages.iterator(); it.hasNext(); ) {
            models.Package pack = it.next();
            if (pack.order.orderStatus != OrderStatus.DONE) {
                it.remove();
                continue;
            }
            pack.order = null;
            pack.statuses = null;
        }

        return ok(Json.toJson(packages));
    }
}
