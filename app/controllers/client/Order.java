package controllers.client;

import models.Client;
import models.OrderStatus;
import models.PackageStatus;
import models.PackageStatusName;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.Date;
import java.util.List;

@Security.Authenticated(Secured.class)
public class Order extends Controller {
    public static Result viewAll() {
        Client client = Client.findByLogin(session("login"));
        List<models.Order> orders = models.Order.find.where().eq("client_id", client.id).findList();

        return ok(views.html.client.orders.render(orders));
}

    public static Result delete(int id) {
        models.Order.findById(id).delete();
        return redirect(request().getHeader("referer"));
    }

    public static Result accept(int id) {
        models.Order order = models.Order.findById(id);
        if (order.orderStatus == OrderStatus.NEW) {
            order.orderStatus = OrderStatus.READY;
            for ( models.Package pack : order.packages ) {
                PackageStatus status = new PackageStatus();
                status.date = new Date();
                status.name = PackageStatusName.NEW;
                status.package_ = pack;

                pack.statuses.add( status );
            }
            order.save();
        }
        return redirect(request().getHeader("referer"));
    }

    public static Result view( int id ) {
        Client client = Client.findByLogin(session("login"));
        models.Order order = models.Order.find.where().eq("id", id).findUnique();
        if (order == null) return redirect(routes.Order.viewAll());

        return ok(views.html.client.order.render(order));
    }

    public static Result addForm() {
        Form<models.Order> form = Form.form(models.Order.class);
        return ok(views.html.client.newOrderForm.render(form));
    }

    public static Result add() {
        Client client = Client.findByLogin(session("login"));

        Form<models.Order> form  = Form.form(models.Order.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(views.html.client.newOrderForm.render(form));
        } else {
            models.Order order = form.get();
            order.client = client;
            order.orderStatus = OrderStatus.NEW;
            order.orderDate = new Date();
            order.save();

            flash("message", "Zamówienie utworzone");
            return redirect(controllers.client.routes.Order.view(order.id));
        }
    }
}
