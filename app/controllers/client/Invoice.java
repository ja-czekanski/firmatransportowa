package controllers.client;

import models.*;
import models.Order;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.Date;
import java.util.List;

@Security.Authenticated(Secured.class)
public class Invoice extends Controller {
    public static Result viewAll() {
        Client client = Client.findByLogin(session("login"));
        List<Order> list = Order.find.where().eq("client_id", client.id).where().isNotNull("invoice_number").findList();
        return ok(views.html.client.invoices.render(list));
    }

    public static Result view(int id) {
        Order order = Order.findById(id);
        return ok(views.html.shared.invoice.render(order));
    }

    public static Result issue(int id) {
        models.Order order = models.Order.findById(id);
        if (order.invoiceNumber == null) {
            int invoiceNumber = 1;
            order.invoiceDate = new Date();

            List<Order> list = Order.find.where().isNotNull("invoice_number").order().desc("invoice_number").findList();
            int maxInvoiceNumber = 1;
            for (Order o : list) {
                invoiceNumber = Integer.parseInt(o.invoiceNumber)+1;
                if (invoiceNumber > maxInvoiceNumber) maxInvoiceNumber = invoiceNumber;
            }
            order.invoiceNumber = ""+maxInvoiceNumber;

            order.save();
        }
        return redirect(request().getHeader("referer"));
    }
}
