package controllers.client;

import models.Client;
import models.OrderStatus;
import models.PackageType;
import models.Recipient;
import play.data.validation.ValidationError;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import scala.math.BigDecimal;

import javax.xml.bind.ValidationEvent;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Security.Authenticated(Secured.class)
public class Package extends Controller {
    public static Result viewAll() {
        Client client = Client.findByLogin(session("login"));
        List<models.Package> packages = new ArrayList<>();

        for (models.Package pack : models.Package.find.all()) {
            if (pack.order.client.id == client.id && !pack.statuses.isEmpty()) {
                packages.add(pack);
            }
        }

        return ok(views.html.client.packages.render(packages));
    }

    public static Result view(int id) {
        models.Package pack = models.Package.findById(id);
        return ok(views.html.client.pack.render(pack));
    }

    public static Result delete( int id ) {
        models.Package.findById(id).delete();
        return redirect(request().getHeader("referer"));
    }

    public static Result addForm( int orderId ) {
        DynamicForm form = Form.form();
        return ok(views.html.client.newPackageForm.render(form, PackageType.options(), orderId));
    }

    public static Result add( int orderId ) {
        Client client = Client.findByLogin(session("login"));

        DynamicForm form = Form.form().bindFromRequest();
        models.Package pack = new models.Package();

        pack.name = form.get("name");

        String weight = form.get("weight");
        if (weight != null) {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setGroupingSeparator(',');
            symbols.setDecimalSeparator('.');
            String pattern = "#,##0.0#";
            DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
            decimalFormat.setParseBigDecimal(true);

            try {
                pack.weight = (java.math.BigDecimal) decimalFormat.parse(weight);
            } catch (ParseException e) {
            }
        }


        String packageTypeId = form.get("packageType.id");
        if (packageTypeId != null) {
            int typeId = 0;
            try {
                typeId = Integer.parseInt(packageTypeId);

                if (typeId > 0 && typeId <= PackageType.find.findRowCount())
                    pack.packageType = PackageType.findById(typeId);
                else form.reject("packageType.id", "Nieprawidłowy rodzaj przesyłki");
            } catch (NumberFormatException e) {
                form.reject("packageType.id", "Wybierz rodzaj przesyłki");
            }
        }
        else form.reject("packageType.id", "Wybierz rodzaj przesyłki");

        {
            List<ValidationError> errors = pack.validate();
            if (errors != null) {
                for(ValidationError error : errors) {
                    form.reject(error.key(), error.message());
                }
            }
        }

        String recipientId = form.get("recipient_id");
        if ( recipientId != null && !recipientId.isEmpty()) {
            int recipient = Integer.parseInt(recipientId);
            if (recipient > 0) {
                pack.recipient = Recipient.findById(recipient);
            }
        }

        if (pack.recipient == null) {
            pack.recipient = new Recipient();
            pack.recipient.name = form.get("recipient_name");
            pack.recipient.surname = form.get("recipient_surname");
            pack.recipient.company = form.get("recipient_company");
            pack.recipient.street = form.get("recipient_street");
            pack.recipient.streetNumber = form.get("recipient_streetNumber");
            pack.recipient.city = form.get("recipient_city");
            pack.recipient.zipcode = form.get("recipient_zipcode");
            pack.recipient.phoneNumber = form.get("recipient_phoneNumber");

            List<ValidationError> errors = pack.recipient.validate();
            if (errors != null) {
                for(ValidationError error : errors) {
                    form.reject("recipient_"+error.key(), error.message());
                }
            }
        }

        if (form.hasErrors()) {
            return badRequest(views.html.client.newPackageForm.render(form, PackageType.options(), orderId));
        } else {
            pack.order = models.Order.findById(orderId);
            // TODO: Add package status
            pack.save();

            flash("message", "Przesyłka utworzona");
            return redirect(controllers.client.routes.Order.view(orderId));
        }
    }
}
