package controllers.client;

import controllers.routes;
import models.Client;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.Security;

@Security.Authenticated(Secured.class)
public class Dashboard extends Controller {
    public static Result index() {
        Client client = Client.findByLogin(session("login"));
        return ok(views.html.client.index.render(client));
    }
}
