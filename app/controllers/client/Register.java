package controllers.client;

import models.*;
import models.Order;
import play.api.mvc.Flash;
import play.data.Form;
import play.db.ebean.Model;
import play.mvc.Controller;
import play.mvc.Result;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Register extends Controller {
    public static Result registerationForm() {
        Form<models.Client> form  = Form.form(models.Client.class);
        return ok(views.html.client.registrationForm.render(form));
    }
    public static Result register() {
        Form<models.Client> form  = Form.form(models.Client.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(views.html.client.registrationForm.render(form));
        } else {
            Client client = form.get();
            client.registrationDate = new Date();
            client.save();

            Login.setLoggedUser(client.login);
            flash("message", "Użytkownik stworzony");
            return redirect(controllers.client.routes.Dashboard.index());
        }
    }

}
