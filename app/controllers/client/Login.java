package controllers.client;

import controllers.routes;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class Login extends Controller {

    public static Result login() {
        return ok(views.html.client.login.render(Form.form(LoginValidator.class)));
    }

    public static Result logout() {
        session().clear();
        return redirect(routes.Application.index());
    }

    public static Result authenticate() {
        Form<LoginValidator> loginForm = Form.form(LoginValidator.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(views.html.client.login.render(loginForm));
        } else {
            setLoggedUser(loginForm.get().login);
            return redirect(controllers.client.routes.Dashboard.index());
        }
    }

    public static void setLoggedUser( String login ) {
        session().clear();
        session("login", login);
        session("who", "client");
    }

    public static class LoginValidator {
        public String login;
        public String password;

        public String validate() {
            models.Client client = null;
            try {
                 client = models.Client.authenticate(login, password);
            } catch (models.Client.LoginException e) {
                return e.getMessage();
            }
            return null;
        }
    }
}
