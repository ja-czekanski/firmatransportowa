package controllers.client;

import models.Client;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.Date;

public class Account extends Controller {
    public static Result manage() {
        Client client = Client.findByLogin(session("login"));
        Form<Client> form  = Form.form(Client.class).fill(client);

        return ok(views.html.client.manageAccount.render(form));
    }
    public static Result update() {
        Form<Client> form  = Form.form(Client.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(views.html.client.manageAccount.render(form));
        } else {
            Client client = form.get();
            client.update();

            flash("message", "Dane zaktualizowane");
            return redirect(routes.Dashboard.index());
        }
    }

}
