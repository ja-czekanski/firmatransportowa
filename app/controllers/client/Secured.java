package controllers.client;


import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;

public class Secured extends Security.Authenticator {

    @Override
    public String getUsername(Context ctx) {
        if (ctx.session().getOrDefault("who", "").equals("client")) return ctx.session().get("login");
        return null;
    }

    @Override
    public Result onUnauthorized(Context ctx) {
        return redirect(controllers.client.routes.Login.login());
    }
}
