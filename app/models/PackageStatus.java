package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "package_status")
public class PackageStatus extends Model{
    @Id
    @GeneratedValue
    @Column
    public int id;

    @ManyToOne
    @JoinColumn(name = "package_id")
    public Package package_;

    @Column(name = "package_status_name")
    @Enumerated(EnumType.ORDINAL)
    public PackageStatusName name;

    @Column
    public Date date;
}

