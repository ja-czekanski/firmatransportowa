package models;

import play.data.validation.ValidationError;
import play.db.ebean.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "package")
public class Package extends Model {
    @Id
    @GeneratedValue
    @Column(name="id")
    public int id;

    @Column
    public String name;

    @Column
    public BigDecimal weight;

    @ManyToOne
    @JoinColumn(name = "order_id")
    public Order order;

    @ManyToOne
    @JoinColumn(name = "track_id")
    public Track track;

    @ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "recipient_id")
    public Recipient recipient;

    @ManyToOne
    @JoinColumn(name = "package_type_id")
    public PackageType packageType;

    @OneToMany(mappedBy = "package_", cascade = CascadeType.ALL)
    public List<PackageStatus> statuses = new ArrayList<PackageStatus>();

    public static Model.Finder<Integer, Package> find = new Model.Finder<>(Integer.class, Package.class);

    public static Package findById(int id) {
        return find.where().eq("id", id).findUnique();
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();
        if (name.length() < 4) errors.add(new ValidationError("name", "Za krótka nazwa"));
        if (weight != null && (weight.floatValue() < 0.1 || weight.floatValue() > 1000.0))  errors.add(new ValidationError("weight", "Nieprawidłowa waga"));
        // TODO: Check packageType

        if (!errors.isEmpty()) return errors;
        return null;
    }

    public BigDecimal calculatePrice() {
        BigDecimal price = new BigDecimal(0);
        price = price.add(weight);
        price = price.multiply(packageType.price);
        return price;
    }

    public BigDecimal calculatePriceWithoutTax() {
        BigDecimal price = new BigDecimal(0);
        price = price.add(weight);
        price = price.multiply(packageType.price);
        price = price.multiply(new BigDecimal("0.77"));
        return price;
    }
}
