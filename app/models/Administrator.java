package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "administrator")
public class Administrator extends Model{
    //TODO: id
    @Id
    @Column
    public String login;
    
    @Column
    public String password;

    @Column
    public String name;

    @Column
    public String surname;

    @OneToMany(mappedBy = "administrator")
    public List<Order> orders = new ArrayList<Order>();

    public static Model.Finder<Integer, Administrator> find = new Model.Finder<>(Integer.class, Administrator.class);

    public static Administrator findById(int id) {
        return find.where().eq("id", id).findUnique();
    }

    public static Administrator findByLogin(String login) {
        return find.where().eq("login", login).findUnique();
    }

}
