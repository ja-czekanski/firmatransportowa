package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "city")
public class City extends Model {
    @Id
    @Column
    public Integer id;

    @Column
    public String name;

    @OneToMany(mappedBy = "city")
    public List<Street> streets = new ArrayList<Street>();
}
