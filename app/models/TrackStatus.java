package models;

/**
 * Created by Kuba
 */
public enum TrackStatus {
    NEW {
        @Override
        public String toString() {
            return "Utworzone";
        }
    },
    READY {
        @Override
        public String toString() {
            return "Gotowe do rozpatrzenia";
        }
    },
    DONE {
        @Override
        public String toString() {
            return "Zakończone";
        }
    },
}