package models;

/**
 * Created by Kuba
 */
public enum OrderStatus {
    NEW {
        @Override
        public String toString() {
            return "Utworzone";
        }
    },
    READY {
        @Override
        public String toString() {
            return "Gotowe do rozpatrzenia";
        }
    },
    PROCESSED {
        @Override
        public String toString() {
            return "Przetwarzane";
        }
    },
    TRANSIT {
        @Override
        public String toString() {
            return "W dostarczeniu";
        }
    },
    DONE {
        @Override
        public String toString() {
            return "Zakończone";
        }
    };
}