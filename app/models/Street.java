package models;

import models.City;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "street")
public class Street extends Model {
    @Id
    @GeneratedValue
    @Column
    public int id;

    @Column
    public String name;

    @ManyToOne
    @JoinColumn(name="city_id")
    public City city;
}
