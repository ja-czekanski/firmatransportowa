package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "track")
public class Track extends Model {
    @Id
    @GeneratedValue
    @Column
    public int id;

    @ManyToOne
    @JoinColumn(name = "driver_id")
    public Driver driver;

    @Column(name = "track_status")
    @Enumerated(EnumType.ORDINAL)
    public TrackStatus trackStatus;

    @OneToMany(mappedBy = "track")
    public List<Package> packages = new ArrayList<>();

    public static Model.Finder<Integer, Track> find = new Model.Finder<>(Integer.class, Track.class);

    public static Track findById(int id) {
        return find.where().eq("id", id).findUnique();
    }
}
