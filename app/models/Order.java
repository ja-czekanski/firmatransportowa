package models;

import play.data.validation.ValidationError;
import play.db.ebean.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "order_")
public class Order extends Model {
    @Id
    @GeneratedValue
    @Column
    public int id;

    @Column
    public String name;

    @ManyToOne
    @JoinColumn(name = "client_id")
    public Client client;

    @ManyToOne
    @JoinColumn(name = "administrator_id")
    public Administrator administrator;

    @Column(name = "order_date")
    public Date orderDate;

    @Column(name = "order_status")
    @Enumerated(EnumType.ORDINAL)
    public OrderStatus orderStatus;

    @Column(name = "invoice_number")
    public String invoiceNumber;

    @Column(name = "invoice_date")
    public Date invoiceDate;

    @Column(name = "notes")
    public String notes;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    public List<Package> packages = new ArrayList<>();


    public static Model.Finder<Integer, Order> find = new Model.Finder<>(Integer.class, Order.class);

    public static Order findById(int id) {
        return find.where().eq("id", id).findUnique();
    }


    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();
        if (name.isEmpty()) errors.add(new ValidationError("name", "Zamówienie musi posiadać nazwę"));
        if (name.length() < 5) errors.add(new ValidationError("name", "Nazwa musi być dłuższa niż 5 znaków"));
        Integer.parseInt("1");
        if (!errors.isEmpty()) return errors;
        return null;
    }


    public BigDecimal calculatePrice() {
        BigDecimal price = new BigDecimal(0);
        if (packages != null && !packages.isEmpty()) {
            for (Package p : packages) {
                price = price.add(p.calculatePrice());
            }
        }
        return price;
    }

    public BigDecimal calculatePriceWithoutTax() {
        BigDecimal price = new BigDecimal(0);
        if (packages != null && !packages.isEmpty()) {
            for (Package p : packages) {
                price = price.add(p.calculatePriceWithoutTax());
            }
        }
        return price;
    }
}
