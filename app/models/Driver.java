package models;

import play.data.validation.ValidationError;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "driver")
public class Driver extends Model {

    @Id
    @GeneratedValue
    @Column(name = "id")
    public int id;

    @Column
    public String car;

    @Column
    public String name;

    @Column
    public String surname;

    @Column
    public String login;

    @Column
    public String password;


    public static Model.Finder<Integer, Driver> find = new Model.Finder<>(Integer.class, Driver.class);

    public static Driver findById(int id) {
        return find.where().eq("id", id).findUnique();
    }

    public static Driver findByLogin(String login) {
        return find.where().eq("login", login).findUnique();
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();
        if (login.length() < 4) errors.add(new ValidationError("login", "Nieprawidłowy login"));
        if (password.length() < 6)  errors.add(new ValidationError("password", "Nieprawidłowe hasło"));;
        if (name.length() < 2) errors.add(new ValidationError("name", "Nieprawidłowe imie"));
        if (surname.length() < 3) errors.add(new ValidationError("surname", "Nieprawidłowe nazwisko"));
        if (car.length() < 7) errors.add(new ValidationError("car", "Nieprawidłowa rejestracja"));

        if (!errors.isEmpty()) return errors;
        return null;
    }
}
