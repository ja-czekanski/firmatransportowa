package models;

import play.data.validation.ValidationError;
import play.db.ebean.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "package_type")
public class PackageType extends Model {
    @Id
    @GeneratedValue
    @Column
    public int id;

    @Column
    public String name;

    @Column
    public BigDecimal price;

    @Column(columnDefinition = "TEXT")
    public String description;


    public static Model.Finder<Integer, PackageType> find = new Model.Finder<>(Integer.class, PackageType.class);

    public static PackageType findById(int id) {
        return find.where().eq("id", id).findUnique();
    }

    public static Map<Integer, String> options() {
        List<PackageType> types = find.all();
        LinkedHashMap<Integer, String> map = new LinkedHashMap<>();

        for (PackageType type : types) {
            map.put(type.id, type.name);
        }
        return map;
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();
        if (name == null || name.length() < 5) errors.add(new ValidationError("name", "Za krótka nazwa"));
        if (price == null || price.intValue() < 1 || price.intValue() > 999)  errors.add(new ValidationError("price", "Nieprawidłowa cena"));

        if (!errors.isEmpty()) return errors;
        return null;
    }
}
