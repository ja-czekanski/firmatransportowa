package models;

import javax.persistence.*;

/**
 * Created by Kuba
 */
public enum PackageStatusName {
    NEW {
        @Override
        public String toString() {
            return "Utworzono zamówienie";
        }
    },
    WAITINGFORCLIENT {
        @Override
        public String toString() {
            return "Czeka na przesyłkę";
        }
    },
    RECEIVED {
        @Override
        public String toString() {
            return "Odebrane przez kuriera";
        }
    },
    TRANSIT {
        @Override
        public String toString() {
            return "W dostarczeniu";
        }
    },
    WAITINGFORRECIPIENT {
        @Override
        public String toString() {
            return "Wydano do dostarczenia";
        }
    },
    DONE {
        @Override
        public String toString() {
            return "Dostarczono";
        }
    };

    static PackageStatusName getStringById( int id ) {
        switch (id) {
            case 0: return NEW;
            case 1: return WAITINGFORCLIENT;
            case 2: return RECEIVED;
            case 3: return TRANSIT;
            case 4: return WAITINGFORRECIPIENT;
            case 5: return DONE;
            default: return DONE;
        }
    }
}