package models;

import play.data.validation.ValidationError;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "recipient")
public class Recipient extends Model {
    @Id
    @GeneratedValue
    @Column
    public int id;

    @Column
    public String name;

    @Column
    public String surname;

    @Column
    public String company;

    @Column
    public String street;

    @Column(name="street_number")
    public String streetNumber;

    @Column
    public String city;

    @Column(name="zip_code")
    public String zipcode;

    @Column(name="phone_number")
    public String phoneNumber;

    public static Model.Finder<Integer, Recipient> find = new Model.Finder<>(Integer.class, Recipient.class);

    public static Recipient findById(int id) {
        return find.where().eq("id", id).findUnique();
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();
        if (name.length() < 2) errors.add(new ValidationError("name", "Nieprawidłowe imie"));
        if (surname.length() < 3) errors.add(new ValidationError("surname", "Nieprawidłowe nazwisko"));
        if (street.length() < 2) errors.add(new ValidationError("street", "Nieprawidłowa ulica"));
        if (streetNumber.length() < 1) errors.add(new ValidationError("streetNumber", "Nieprawidłowy numer domu"));
        if (city.length() < 3) errors.add(new ValidationError("city", "Nieprawidłowe miasto"));
        if (!zipcode.matches("\\d{2}-\\d{3}")) errors.add(new ValidationError("zipcode", "Nieprawidłowy kod pocztowy"));
        if (phoneNumber.length() < 9)  errors.add(new ValidationError("phoneNumber", "Nieprawidłowy numer telefonu"));

        if (!errors.isEmpty()) return errors;
        return null;
    }
}
