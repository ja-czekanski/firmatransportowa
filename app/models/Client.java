package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.sun.javafx.fxml.expression.Expression;
import play.data.validation.ValidationError;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Kuba
 */
@Entity
@Table(name = "client")
public class Client extends Model{
    @Id
    @GeneratedValue
    @Column
    public int id;

    //@Constraints.Required
    //@Formats.NonEmpty
    @Column
    public String login;

    //@Constraints.Required
    //@Formats.NonEmpty
    @Column
    public String password;

    @Column
    public String company;

    @Column
    public String regon;

    @Column
    public String nip;

    @Column
    public String name;

    @Column
    public String surname;

    @Column
    public String street;

    @Column(name = "street_number")
    public String streetNumber;

    @Column
    public String city;

    @Column(name = "zip_code")
    public String zipcode;

    @Column
    public String email;

    @Column(name = "phone_number")
    public String phoneNumber;

    @Column(name = "registration_date")
    public Date registrationDate;

    @OneToMany(mappedBy = "client")
    public List<Order> orders = new ArrayList<Order>();

    public static Model.Finder<Integer, Client> find = new Model.Finder<>(Integer.class, Client.class);

    public static Client findById(int id) {
        return find.where().eq("id", id).findUnique();
    }

    public static Client findByLogin(String login) {
        return find.where().eq("login", login).findUnique();
    }

    public static Client authenticate(String login, String password) throws LoginException {
        Client client = find.where().eq("login", login).findUnique();
        if (client == null) throw new LoginException("Nieprawidłowy login");
        if (!client.password.equals(password)) throw new LoginException("Nieprawidłowe hasło");
        return client;
    }

    public static class LoginException extends Exception {
        public LoginException(String message) {
            super(message);
        }
    }


    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();
        if (login.length() < 4) errors.add(new ValidationError("login", "Nieprawidłowy login"));
        //if (Client.findByLogin(login) != null) errors.add(new ValidationError("login", "Użytkownik istnieje"));
        if (password.length() < 6)  errors.add(new ValidationError("password", "Nieprawidłowe hasło"));;
        if (name.length() < 2) errors.add(new ValidationError("name", "Nieprawidłowe imie"));
        if (surname.length() < 3) errors.add(new ValidationError("surname", "Nieprawidłowe nazwisko"));
        if (street.length() < 2) errors.add(new ValidationError("street", "Nieprawidłowa ulica"));
        if (streetNumber.length() < 1) errors.add(new ValidationError("streetNumber", "Nieprawidłowy numer domu"));
        if (city.length() < 3) errors.add(new ValidationError("city", "Nieprawidłowe miasto"));
        if (!zipcode.matches("\\d{2}-\\d{3}")) errors.add(new ValidationError("zipcode", "Nieprawidłowy kod pocztowy"));
        if (!email.matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")) errors.add(new ValidationError("email", "Nieprawidłowy adres email"));
        if (phoneNumber.length() < 9)  errors.add(new ValidationError("phoneNumber", "Nieprawidłowy numer telefonu"));

        if (!errors.isEmpty()) return errors;
        return null;
    }
}
