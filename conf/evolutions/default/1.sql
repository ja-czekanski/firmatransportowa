# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table administrator (
  login                     varchar(255) not null,
  password                  varchar(255),
  name                      varchar(255),
  surname                   varchar(255),
  constraint pk_administrator primary key (login))
;

create table city (
  id                        integer not null,
  name                      varchar(255),
  constraint pk_city primary key (id))
;

create table client (
  id                        integer not null,
  login                     varchar(255),
  password                  varchar(255),
  company                   varchar(255),
  regon                     varchar(255),
  nip                       varchar(255),
  name                      varchar(255),
  surname                   varchar(255),
  street                    varchar(255),
  street_number             varchar(255),
  city                      varchar(255),
  zip_code                  varchar(255),
  email                     varchar(255),
  phone_number              varchar(255),
  registration_date         timestamp,
  constraint pk_client primary key (id))
;

create table driver (
  id                        integer not null,
  car                       varchar(255),
  name                      varchar(255),
  surname                   varchar(255),
  login                     varchar(255),
  password                  varchar(255),
  constraint pk_driver primary key (id))
;

create table order_ (
  id                        integer not null,
  name                      varchar(255),
  client_id                 integer,
  administrator_id          varchar(255),
  order_date                timestamp,
  order_status              integer,
  invoice_number            varchar(255),
  invoice_date              timestamp,
  notes                     varchar(255),
  constraint ck_order__order_status check (order_status in (0,1,2,3,4)),
  constraint pk_order_ primary key (id))
;

create table package (
  id                        integer not null,
  name                      varchar(255),
  weight                    decimal(38),
  order_id                  integer,
  track_id                  integer,
  recipient_id              integer,
  package_type_id           integer,
  constraint pk_package primary key (id))
;

create table package_status (
  id                        integer not null,
  package_id                integer,
  package_status_name       integer,
  date                      timestamp,
  constraint ck_package_status_package_status_name check (package_status_name in (0,1,2,3,4,5)),
  constraint pk_package_status primary key (id))
;

create table package_type (
  id                        integer not null,
  name                      varchar(255),
  price                     decimal(38),
  description               TEXT,
  constraint pk_package_type primary key (id))
;

create table recipient (
  id                        integer not null,
  name                      varchar(255),
  surname                   varchar(255),
  company                   varchar(255),
  street                    varchar(255),
  street_number             varchar(255),
  city                      varchar(255),
  zip_code                  varchar(255),
  phone_number              varchar(255),
  constraint pk_recipient primary key (id))
;

create table street (
  id                        integer not null,
  name                      varchar(255),
  city_id                   integer,
  constraint pk_street primary key (id))
;

create table track (
  id                        integer not null,
  driver_id                 integer,
  track_status              integer,
  constraint ck_track_track_status check (track_status in (0,1,2)),
  constraint pk_track primary key (id))
;

create sequence administrator_seq;

create sequence city_seq;

create sequence client_seq;

create sequence driver_seq;

create sequence order__seq;

create sequence package_seq;

create sequence package_status_seq;

create sequence package_type_seq;

create sequence recipient_seq;

create sequence street_seq;

create sequence track_seq;

alter table order_ add constraint fk_order__client_1 foreign key (client_id) references client (id);
create index ix_order__client_1 on order_ (client_id);
alter table order_ add constraint fk_order__administrator_2 foreign key (administrator_id) references administrator (login);
create index ix_order__administrator_2 on order_ (administrator_id);
alter table package add constraint fk_package_order_3 foreign key (order_id) references order_ (id);
create index ix_package_order_3 on package (order_id);
alter table package add constraint fk_package_track_4 foreign key (track_id) references track (id);
create index ix_package_track_4 on package (track_id);
alter table package add constraint fk_package_recipient_5 foreign key (recipient_id) references recipient (id);
create index ix_package_recipient_5 on package (recipient_id);
alter table package add constraint fk_package_packageType_6 foreign key (package_type_id) references package_type (id);
create index ix_package_packageType_6 on package (package_type_id);
alter table package_status add constraint fk_package_status_package__7 foreign key (package_id) references package (id);
create index ix_package_status_package__7 on package_status (package_id);
alter table street add constraint fk_street_city_8 foreign key (city_id) references city (id);
create index ix_street_city_8 on street (city_id);
alter table track add constraint fk_track_driver_9 foreign key (driver_id) references driver (id);
create index ix_track_driver_9 on track (driver_id);



# --- !Downs

drop table if exists administrator cascade;

drop table if exists city cascade;

drop table if exists client cascade;

drop table if exists driver cascade;

drop table if exists order_ cascade;

drop table if exists package cascade;

drop table if exists package_status cascade;

drop table if exists package_type cascade;

drop table if exists recipient cascade;

drop table if exists street cascade;

drop table if exists track cascade;

drop sequence if exists administrator_seq;

drop sequence if exists city_seq;

drop sequence if exists client_seq;

drop sequence if exists driver_seq;

drop sequence if exists order__seq;

drop sequence if exists package_seq;

drop sequence if exists package_status_seq;

drop sequence if exists package_type_seq;

drop sequence if exists recipient_seq;

drop sequence if exists street_seq;

drop sequence if exists track_seq;

