name := """play-firmatransportowa"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaCore,
  javaJdbc,
  cache,
  javaWs,
  javaJpa,
  javaEbean,
  //"org.hibernate" % "hibernate-entitymanager" % "4.3.7.Final",
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc4"
)

